﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lenient_Window_Resize
{
    public class ModConfig
    {
        public int MinW { get; set; }
        public int MinH { get; set; }

        public ModConfig()
        {
            MinW = 640;
            MinH = 360;
        }

    }
}
